const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const mysql = require('mysql');

const db = mysql.createPool({
	host: "127.0.0.1",
	user: "antares",
	password: "distance5545",
	database: "so_cooc",
});
//TEST CONNECTION
//app.get("/", (req, res) => {
// db.getConnection(function (err) {
// 	if (err) {
// 		return console.error('error: ' + err.message);
// 	}
// 	console.log('Connected to the MySQL server.');
// });
//});
app.use(cors());
app.use(express.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.post("/api/insert", (req, res) => {

	const data = req.body.data;

	const insertSql = "INSERT INTO participant (gender, firstname, lastname, email, city, fonction, origin, food_restrictions, comments) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

	try {
		db.query(insertSql, [data.gender, data.firstname, data.lastname, data.email, data.city, data.fonction, data.origin, data.food_restrictions, data.comments])

	} catch (err) {
		console.log(err.message);
	}
});

app.listen(3001, () => {
	console.log('Running on port 3001')
});