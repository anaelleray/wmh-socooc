//App with form and footer
import { Container, Form, Row, Col } from 'react-bootstrap';
import { React, useState } from 'react';
// import { useForm } from "react-hook-form";
import Axios from 'axios';
import Button from 'react-bootstrap/Button';
import Router from './Component/Router';
import './App.css'

function App() {

  //Init form data 
  const [firstname, setFirstName] = useState(null);
  const [lastname, setLastName] = useState(null);
  const [email, setEmail] = useState(null);
  const [city, setCity] = useState(null);
  const [comments, setComments] = useState(null);
  const [gender, setGender] = useState(null);
  const [food_rest, setFoodRest] = useState(null);
  const [origin, setOrigin] = useState(null);
  const [fonction, setFonction] = useState(null);

  /////////////////////////////////////////////////////////////
  //TRY REACT—HOOK—FORM FOR VALIDATE/ERROR ON FORM
  //FIXME : INVALID HOOK !
  // const { validate, errors } = useForm();
  // const handleError = (errors) => { };
  // const registerOptions = {
  //   name: { required: "Name is required" },
  //   email: { required: "Email is required" },
  //   password: {
  //     required: "Password is required",
  //     minLength: {
  //       value: 8,
  //       message: "Password must have at least 8 characters"
  //     }
  //   }
  // }
  //INNER FIELD :
  //innerRef={validate(registerOptions.lastname)}
  //{errors.firstname && errors.firstname.message}
  /////////////////////////////////////////////////////////////


  const getFormData = () => {

    //set string to bool for food_restrictions
    let bool_foodRes;
    if (food_rest !== "" && food_rest !== null && food_rest !== undefined) {
      bool_foodRes = JSON.parse(food_rest.toLowerCase())
    }
    //Init data object for the server
    let data = {
      gender: gender,
      firstname: firstname,
      lastname: lastname,
      fonction: fonction,
      email: email,
      city: city,
      origin: origin,
      food_restrictions: bool_foodRes,
      comments: comments,
    }
    //Check data
    if (data.firstname === null || data.lastname === null || data.email === null || data.city === null || data.food_restrictions === null || data.food_restrictions === undefined) {
      alert('Veuillez renseigner tous les champs obligatoires');

    } else {
      if (data.food_restrictions === true && data.comments === null) {
        alert("Si vous avez des restrictions alimentaires, veuillez les préciser dans le champ ci-dessous");

      } else {
        //Send to Server with axios
        Axios.post('http://localhost:3001/api/insert', {
          data: data
        });
      }
    }
  }

  return (
    <div className="App">
      <Router></Router>
      <Container fluid id="container-form">
        <Row id="row-inscription-img" >
          <img alt="" id="img-inscription" src={'./assets/Inscription.png'} ></img>
        </Row>
        <Row id="row-form">
          <Form onSubmit={getFormData} id="form">
            <Col id="col-check-gender">
              {['radio'].map((type) => (
                <div key={`inlineGD-${type}`}>
                  <label class="label-radio">Mme
                    <input
                      class="checkmark check-gender"
                      value="Mme"
                      inline
                      name="gender"
                      type={type}
                      onChange={e => setGender(e.target.value)}
                      id={`inlineGD-${type}-1`}
                    />
                    <span class="checkmark"></span>
                  </label>
                  <label class="label-radio">Mr.
                    <input
                      class="checkmark check-gender"
                      value="Mr"
                      onChange={e => setGender(e.target.value)}
                      inline
                      name="gender"
                      type={type}
                      id={`inlineGD-${type}-2`}
                    />
                    <span class="checkmark"></span>
                  </label>
                </div>
              ))}
            </Col>
            <Col>
              <Form.Group controlId="" id="group-input1" >
                <input type="text" name="firstname" placeholder="Prénom*" class="input-text"
                  onChange={e => setFirstName(e.target.value)}
                />
                <input type="text" name="lastname" placeholder="Nom*" class="input-text"
                  onChange={e => setLastName(e.target.value)}
                />
                {/* FIXME */}
                <Form.Select aria-label="" id="select-fonction">
                  <option onChange={e => setFonction(e.target.value)}>Fonction</option>
                </Form.Select>
                {/* FIXME */}
              </Form.Group>
            </Col>
            <Col>
              <Form.Group controlId="" id="group-input2">
                <input type="email" name="email" placeholder="E-mail*" class="input-text"
                  onChange={e => setEmail(e.target.value)}
                />
                <input type="text" name="city" placeholder="Ville*" class="input-text" onChange={e => setCity(e.target.value)}
                />
                {/* FIXME */}
                <Form.Select aria-label="Default select example" id="select-origin" class="select-css" onChange={e => setOrigin(e.target.value)}>
                  <option>Provenance</option>
                  <option value="Magasin franchisé">Magasin franchisé</option>
                  <option value="Magasin succursale">Magasin succursale</option>
                  <option value="Siège Annecy">Siège Annecy</option>
                  <option value="Siège Fournier">Siège Fournier</option>
                  <option value="Siège Externe">Siège Externe</option>
                </Form.Select>
                {/* FIXME */}
              </Form.Group>
            </Col>
            <Col id="col-FR">
              <Form.Label id="text-restrictions">Avez-vous des<br />contraintes alimentaires?*<br /></Form.Label>
              {['radio'].map((type) => (
                <div key={`inlineFR-${type}`} id="divKey-checkFR">
                  <label class="label-radio">Oui
                    <input
                      class="checkmark"
                      value={true}
                      inline
                      name="food_rest"
                      type={type}
                      onChange={e => setFoodRest((e.target.value))}
                      id={`inlineFR-${type}-1`}
                    />
                    <span class="checkmark"></span>
                  </label>
                  <label class="label-radio">Non
                    <input
                      class="checkmark"
                      value={false}
                      onChange={e => setFoodRest(e.target.value)}
                      inline
                      name="food_rest"
                      type={type}
                      id={`inlineFR-${type}-2`}
                    />
                    <span class="checkmark"></span>
                  </label>
                </div>
              ))}
            </Col>
            <Form.Control
              as="textarea"
              name="comments"
              placeholder="écrivez-ici"
              id="comments-area"
              onChange={(e => setComments(e.target.value))}
            />
            <Row>
              <Col>
                <p id="text-legal">*Mentions obligatoires.<br />Ces données sont exclusivement exploitées dans le cadre du green Camp et ne seront ni diffusées, ni vendues.</p>
              </Col>
              <Col id="col-btn-submit">
                <p class="alert-submit"></p>
                <Button id="btn-submit" onClick={getFormData} type="submit">
                  JE VALIDE
                </Button>
              </Col>
            </Row>
          </Form>
        </Row>

        <Row id="row-footer">
          <Col id="col-footer">
            <img alt="" id="img-footer" src={'./assets/Logo Green camp.png'}></img>
          </Col>
        </Row>

      </Container>
    </div >
  );
}
export default App;