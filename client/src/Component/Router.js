//Router navigation et element du Header
import React from "react";
import { Row, Col, Container, Dropdown } from "react-bootstrap";
import {
	BrowserRouter as Router,
	Routes,
	Link,
} from "react-router-dom";
import '../style/header.css';

export default function Navbar() {
	return (

		<Container fluid id="container-header">
			<Row id="row-header">
				<Router>
					{/* //Menu responsive uniquement sur mobile */}
					<Col xs={6} id="col-menu-dropdown">
						<Dropdown>
							<Dropdown.Toggle variant="success" id="dropdown-basic" id="dropdown-button">
								<img alt="" src="./assets/bar2.png"  id="dropdown-menu"></img>
							</Dropdown.Toggle>
							<Dropdown.Menu id="dropdown-list">
								<Dropdown.Item><Link to="/home" class="custom-list">ACCUEIL</Link></Dropdown.Item>
								<Dropdown.Item><Link to="/inscription" class="custom-list">INSCRIPTION</Link></Dropdown.Item>
								<Dropdown.Item><Link to="/infos-pratiques" class="custom-list">INFOS PRATIQUES</Link></Dropdown.Item>
							</Dropdown.Menu>
						</Dropdown>
					</Col>
					{/* //Logo */}
					<Col xs={6}>
						<img src={'./assets/Logo Socooc.png'} alt="" id="logo"></img>
					</Col>
					{/* //Router desktop */}
					<Col xs={6}>
						<ul id="list">
							<li>
								<Link to="/home" class="list-items" >ACCUEIL</Link>
							</li>
							<li>
								<Link to="/inscription" class="list-items">INSCRIPTION</Link>
							</li>
							<li>
								<Link to="/infos-pratiques" class="list-items">INFOS PRATIQUES</Link>
							</li>
						</ul>
						<Routes>
							{/* <Route exact path="/" element={<App />}>
					</Route> */}
							{/* <Route path="/home" element={<App />}> 
					</Route> */}
						</Routes>
					</Col>
				</Router>
			</Row>
			{/* //Element du Header */}
			<Row id="row-element">
				<Col id="col-greenCamp">
					<img alt="" src={'./assets/Logo Greencamp v2.png'} id="img-greenCamp" ></img>
				</Col>

				<Col id="col-boxText">
					<h1 id="box-text">ÇA TOMBE<br /> À PIC, ON VISE<br /> LE SOMMET.</h1>
				</Col>
			</Row>
		</Container >
	);
}