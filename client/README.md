Projet Formulaire pour WMH : 

Application Client/Server en React (avec react router, react boostrap) pour le Front et node pour le Back. 
Les données sont envoyées en "post" avec Axios.
Base de données MySql.

Start :
npm install
npm start dans le dossier client pour lancer le projet sur le navigateur
nodemon index.js dans le dossier server pour lancer le server 
Changer les identifiants de base de données

